#!/bin/bash

set -eo pipefail

SELF="`readlink $0 2>/dev/null || echo $0`"
SELF="`dirname $SELF`"
SELF="`cd $SELF; pwd`"

. $SELF/../common.sh

IMAGE=demo

if [ $UID -eq 0 ]; then
    echo
    echo "Error: running as root, run as a regular user"
    echo
    exit 1
fi

CONTAINER=${CONTAINER:-$IMAGE}


#---------------


if [ "$#" -lt 1 ]; then
    OPTS="$OPTS -d"
    EXTRA_PARAMS="$INDEX"
else
    OPTS="$OPTS--rm=true -ti"
    EXTRA_PARAMS="$*"
fi

# Mount $BASEDIR
# This way users can use this container to run their tests
# without needing to copy it or clone a fresh copy.
BASEDIR=$(readlink -f $SELF/../..)

mkdir -p $ASSETS

if ! docker run \
     --privileged \
     --volume $ASSETS:$ASSETS \
     --volume $BASEDIR:$BASEDIR \
     --volume $HOME:/home/host_${USER}_home \
     --volume /var/run/docker.sock:/var/run/docker.sock \
     $OPTS --name $CONTAINER --hostname $CONTAINER $IMAGE $EXTRA_PARAMS > /dev/null
then
    echo
    echo "ERROR: could not run container $CONTAINTER from image $IMAGE"
    echo
    exit 1
fi

IPADDRESS=$($SELF/../getip $CONTAINER)
USER=$(id -un)

# need buildbot keys for scp in buildbot-ruby-gems.sh
for f in $HOME/.ssh/id_* ; do docker cp $f $CONTAINER:$HOME/.ssh/ ; done
for f in $HOME/.ssh/id_* ; do docker exec $CONTAINER bash -c "chown $USER:$USER $f" ; done

ssh-keygen -q -f $HOME/.ssh/known_hosts -R $IPADDRESS || true
docker exec -u $USER:docker $CONTAINER bash -c "echo export DISPLAY=$DISPLAY >> $HOME/.bashrc"
docker exec -u $USER:docker $CONTAINER bash -c "echo export TERM=xterm >> $HOME/.bashrc"

# Move bash candy into place (make working inside the container more plesant)
    # .alias
        docker exec $CONTAINER bash -c "cp /tmp/resources/bashCandy/.alias $HOME/ && chown $USER:$USER $HOME/.alias"
        docker exec $CONTAINER bash -c "echo 'source ~/.alias' >> $HOME/.profile"
    # .bash_prompt
        docker exec $CONTAINER bash -c "cp /tmp/resources/bashCandy/.bash_prompt $HOME/ && chown $USER:$USER $HOME/.bash_prompt"
        docker exec $CONTAINER bash -c "echo 'source ~/.bash_prompt' >> $HOME/.profile"
    # add load confirmation
        docker exec $CONTAINER bash -c "echo 'echo '~/.profile loaded'' >> $HOME/.profile"

echo
echo "Started container: $CONTAINER"
echo
echo "   ip address: $IPADDRESS"
echo "     hostname: $CONTAINER"
echo
echo "To login to the container use one of the following:"
echo
echo "    ssh $IPADDRESS"
echo "    docker exec -ti $CONTAINER bash"
echo "    docker exec -ti -u $USER:docker $CONTAINER bash"
echo
echo "To remove the container use:"
echo
echo "    docker rm -f $CONTAINER"
echo

exit 0
